import React from 'react';
import reactDom from 'react-dom';
import PictureGrid from './components/pictureGrid/pictureGrid';

reactDom.render(
    <React.StrictMode>
        <PictureGrid />
    </React.StrictMode>,
    document.getElementById('root')
);