import React, { useState } from 'react';
import { createUseStyles } from 'react-jss'

import image1 from '../../img/photo-1.jpg'
import image2 from '../../img/photo-2.jpg'
import image3 from '../../img/photo-3.jpg'
import image4 from '../../img/photo-4.jpg'

const PictureGrid = (props) => {
    const [itemInput, setItemInput] = useState(null);

    const images = {
        "data": [
            {
                link: "https://www.shutterstock.com/image-photo/mountains-during-sunset-beautiful-natural-landscape-407021107",
                url: image1
            },
            {
                link: "https://www.shutterstock.com/image-photo/autumn-forest-nature-vivid-morning-colorful-766886038",
                url: image1 //image2
            },
            {
                link: "https://www.shutterstock.com/image-photo/large-beautiful-drops-transparent-rain-water-668593321",
                url: image1 //image3
            },
            {
                link: "https://www.shutterstock.com/image-photo/mountains-during-sunset-beautiful-natural-landscape-407021107",
                url: image1 //image4
            },
            {
                link: "https://www.shutterstock.com/image-photo/mountains-during-sunset-beautiful-natural-landscape-407021107",
                url: image1
            },
            {
                link: "https://www.shutterstock.com/image-photo/autumn-forest-nature-vivid-morning-colorful-766886038",
                url: image1 //image2
            },
            {
                link: "https://www.shutterstock.com/image-photo/large-beautiful-drops-transparent-rain-water-668593321",
                url: image1 //image3
            },
            {
                link: "https://www.shutterstock.com/image-photo/mountains-during-sunset-beautiful-natural-landscape-407021107",
                url: image1 //image4
            },
            {
                link: "https://www.shutterstock.com/image-photo/mountains-during-sunset-beautiful-natural-landscape-407021107",
                url: image1
            },
            {
                link: "https://www.shutterstock.com/image-photo/autumn-forest-nature-vivid-morning-colorful-766886038",
                url: image1 //image2
            },
            {
                link: "https://www.shutterstock.com/image-photo/large-beautiful-drops-transparent-rain-water-668593321",
                url: image1 //image3
            },
            {
                link: "https://www.shutterstock.com/image-photo/mountains-during-sunset-beautiful-natural-landscape-407021107",
                url: image1 //image4
            },
            {
                link: "https://www.shutterstock.com/image-photo/mountains-during-sunset-beautiful-natural-landscape-407021107",
                url: image1
            },
            {
                link: "https://www.shutterstock.com/image-photo/autumn-forest-nature-vivid-morning-colorful-766886038",
                url: image1 //image2
            },
            {
                link: "https://www.shutterstock.com/image-photo/large-beautiful-drops-transparent-rain-water-668593321",
                url: image1 //image3
            },
            {
                link: "https://www.shutterstock.com/image-photo/mountains-during-sunset-beautiful-natural-landscape-407021107",
                url: image1 //image4
            },

        ]
    };

    const useStyle = createUseStyles({
        pictureContainer: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexWrap: 'wrap',

        },
        pictureWrapper: {
            margin: '1%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            width: '22.70%',
            // border: '1px solid #a4a0c7',
            borderRadius: '3%',
            boxShadow: '0px 2px 3px 2px #a4a0c7',
            transition: 'all 0.2s ease-in-out',
            '& > img': {
                width: '100%',
                borderRadius: '3%',
            },
            '&:hover': {
                boxShadow: '0px 6px 10px 2px #605d7ba6',
                transform: 'translateY(-3px)',
            }
        },
        '@media screen and (max-width: 992px)': {
            pictureWrapper: {
                width: '30.97%',
            },
        },
        '@media screen and (max-width: 576px)': {
            pictureWrapper: {
                width: '90%',
                margin: '5%',
            },
        }
    });


    const classes = useStyle();


    return (
        <div>
            <div className={classes.pictureContainer}>
                {
                    images.data.map(item => <a className={classes.pictureWrapper} href={item.link}><img src={item.url} alt="test" /></a>)
                }
            </div>
        </div>
    )
}



export default PictureGrid;